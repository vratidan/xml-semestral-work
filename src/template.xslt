<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html" encoding="utf-8" indent="yes" />

    <xsl:template match="country">
        <html>
            <head>
                <title></title>
                <link rel='stylesheet' href='styles.css' />
            </head>
            <body>
                <header>
                    <img class='flag'>
                        <xsl:attribute name="src">
                            <xsl:value-of select="//flag"/>
                        </xsl:attribute>
                    </img>
                    <div class='heading'>
                        <h1>
                            <xsl:value-of select="//country-name" />
                        </h1>
                        <h4>
                            <xsl:value-of select="//map-references" />
                        </h4>
                    </div>
                </header>

                <xsl:apply-templates select="introduction" />
                <xsl:apply-templates select="geography" />
                <xsl:apply-templates select='people-and-society' />
                <xsl:apply-templates select='government' />
                <xsl:apply-templates select='economy' />

            </body>
        </html>
    </xsl:template>


    <xsl:template match="introduction">
        <div class="container">
            <h2>Introduction</h2>
            <div>
                <p>
                    <xsl:value-of select="//background" />
                </p>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="geography">
        <div class="container">
            <h2>Geography</h2>
            <div class="section">
                <h4>Location</h4>
                <div>
                    <xsl:value-of select="//location" />
                </div>
            </div>
            <div class="section">
                <h4>Geographic and Coordinates</h4>
                <div>
                    <xsl:apply-templates select="geographic-coordinates" />
                </div>
            </div>

            <div class="section">
                <h4>Map reference:</h4>
                <div>
                    <xsl:value-of select="//map-references" />
                </div>
            </div>

            <div class="section">
                <xsl:apply-templates select="area" />
            </div>

            <div class="section">
                <xsl:apply-templates select="land-boundaries" />
            </div>

            <div class="section">
                <h4>Coastline</h4>
                <div>
                    <xsl:value-of select='//coastline' /><xsl:value-of select='//coastline/@unit' />
                </div>
            </div>

            <div class="section">
                <h4>Climate</h4>
                <div>
                    <xsl:value-of select='//climate' />
                </div>
            </div>

            <div class="section">
                <h4>Terrain</h4>
                <div>
                    <xsl:value-of select='//terrain' />
                </div>
            </div>

            <div class="section">
                <h4>Elevation</h4>
                <div>
                    <xsl:apply-templates select='elevation' />
                </div>
            </div>

            <div class="section">
                <h4>Natural resources</h4>
                <div>
                    <xsl:apply-templates select='natural-resources' />
                </div>
            </div>

            <div class="section">
                <h4>Land uses</h4>
                <div>
                    <xsl:apply-templates select='land-uses' />
                </div>
            </div>

            <div class="section">
                <h4>Population distribution</h4>
                <div>
                   <xsl:value-of select='//population-distribution' />
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="people-and-society">
        <div class="container">
            <h2>People and society</h2>
            
            <div class="section">
                <h4>Population</h4>
                <div>
                    <xsl:value-of select='//population' /> people to <xsl:value-of select='//population/@date' />
                </div>
            </div>

            <div class="section">
                <h4>Ethnic groups</h4><span>(<xsl:value-of select='//ethnic-groups/@year' />)</span>
                <div>
                    <xsl:apply-templates select='//ethnic-groups' />
                </div>
            </div>

            <div class="section">
                <h4>Languages</h4>
                <div>
                    <xsl:apply-templates select="//languages" />
                </div>
            </div>

            <div class="section">
                <h4>Religions</h4>
                <div>
                    <xsl:apply-templates select="//religions" />
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="government">
        <div class="container">
            <h2>Government</h2>
            
            <div class="section">
                <h4>Country name</h4>
                <div>
                    <xsl:value-of select='//country-name' />
                </div>
            </div>

            <div class="section">
                <h4>Government type</h4>
                <div>
                    <xsl:value-of select='//government-type' />
                </div>
            </div>

            <div class="section">
                <h4>Capital</h4>
                <div>
                    <xsl:apply-templates select='//capital' />
                </div>
            </div>

            <div class="section">
                <h4>Administrative divisions</h4>
                <div>
                    <xsl:apply-templates select="//administrative-divisions" />
                </div>
            </div>

            <div class="section">
                <h4>National holiday</h4>
                <div>
                    <xsl:apply-templates select="//national-holiday" />
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="economy">
        <div class="container">
            <h2>Economy</h2>
            
            <div class="section">
                <h4>Economy overview</h4>
                <div>
                    <xsl:apply-templates select="//economy-overview" />
                </div>
            </div>

            <div class="section">
                <h4>Agriculture products</h4>
                <div>
                    <xsl:apply-templates select="//agriculture-products" />
                </div>
            </div>

            <div class="section">
                <h4>Industries</h4>
                <div>
                    <xsl:apply-templates select="//industries" />
                </div>
            </div>
        </div>
    </xsl:template>










    <xsl:template match="industries">
        <ul class='margin'>
                <xsl:apply-templates select="//industries/industry" />
        </ul>
    </xsl:template>

    <xsl:template match="industries/industry">
        <li>
            <xsl:value-of select="." />
        </li>
    </xsl:template>

    <xsl:template match="agriculture-products">
        <ul class='margin'>
                <xsl:apply-templates select="//agriculture-products/agriculture-product" />
        </ul>
    </xsl:template>

    <xsl:template match="agriculture-products/agriculture-product">
        <li>
            <xsl:value-of select="./name" />: <xsl:value-of select="./percentage" />%
        </li>
    </xsl:template>

    <xsl:template match="economy-overview">
        <ul class='margin'>
                <xsl:apply-templates select="//economy-overview/paragraph" />
        </ul>
    </xsl:template>

    <xsl:template match="economy-overview/paragraph">
        <li>
            <xsl:value-of select="." />
        </li>
    </xsl:template>

    <xsl:template match="national-holiday">
        <ul class='margin'>
                <xsl:apply-templates select="//national-holiday/holiday" />
        </ul>
    </xsl:template>

    <xsl:template match="national-holiday/holiday">
        <li>
            <xsl:value-of select="./name" />  -  <xsl:value-of select="./date" />
        </li>
    </xsl:template>

    <xsl:template match="administrative-divisions">
        <ul class='margin'>
                <xsl:apply-templates select="//administrative-divisions/department" />
        </ul>
    </xsl:template>

    <xsl:template match="administrative-divisions/department">
        <li>
            <xsl:value-of select="." />
        </li>
    </xsl:template>

    <xsl:template match="capital">
        Name: <xsl:value-of select='name' />
            <xsl:apply-templates select="geographic-coordinates" />
    </xsl:template>

    <xsl:template match="religions">
        <ul class='margin'>
                <xsl:apply-templates select="//religions/religion" />
        </ul>
    </xsl:template>

    <xsl:template match="religions/religion">
        <li>
            <xsl:value-of select="./name" />: <xsl:value-of select="./percentage" />%
        </li>
    </xsl:template>

    <xsl:template match="ethnic-groups">
        <ul class='margin'>
                <xsl:apply-templates select="//ethnic-groups/ethnic-group" />
        </ul>
    </xsl:template>

    <xsl:template match="ethnic-groups/ethnic-group">
        <li>
            <xsl:value-of select="./type" />: <xsl:value-of select="./percentage" />%
        </li>
    </xsl:template>

    <xsl:template match="land-uses">
        <ul class='margin'>
                <xsl:apply-templates select="//land-uses/land-use" />
        </ul>
    </xsl:template>

    <xsl:template match="land-uses/land-use">
        <li>
            <xsl:value-of select="./name" />: <xsl:value-of select="./percentage" />%
        </li>
    </xsl:template>

    <xsl:template match="natural-resources">
        <ul class='margin'>
                <xsl:apply-templates select="//natural-resources/resource" />
        </ul>
    </xsl:template>

    <xsl:template match="geography/natural-resources/resource">
        <li>
            <xsl:value-of select="." />
        </li>
    </xsl:template>

    <xsl:template match="elevation">
        <ul class='margin'>
            <li>
                Mean elevation: <xsl:value-of select="mean-elevation" /><xsl:value-of select="mean-elevation/@unit" />
            </li>
            <li>
                Lowest point: <xsl:value-of select="lowest-point" /><xsl:value-of select="lowest-point/@unit" />
            </li>
            <li>
                Highest point: <xsl:value-of select="highest-point" /><xsl:value-of select="highest-point/@unit" />
            </li>
        </ul>
    </xsl:template>

    <xsl:template match="land-boundaries">
        <h4>Land boundaries</h4>
        <ul class='margin'>
            <li>
                Total: <xsl:value-of select="total" /><xsl:value-of select="total/@unit" />
            </li>
        </ul>
    </xsl:template>

    <xsl:template match="area">
        <h4>Area</h4>
        <ul class='margin'>
            <li>
                Land: <xsl:value-of select="land" /><xsl:value-of select="land/@unit" />
            </li>
            <li>
                Water: <xsl:value-of select="water" /><xsl:value-of select="water/@unit" />
            </li>
        </ul>
        
    </xsl:template>

    <xsl:template match="geographic-coordinates">
        
        <ul class='margin'>
            <li>
                <span>
                    <xsl:value-of select="latitude/degrees" />
                    °
                </span>
                <span>
                    <xsl:value-of select="latitude/minutes" />
                    ″
                </span>
                <span>
                    <xsl:value-of select="latitude/@orientation" />
                </span>
            </li>
            <li>
                <span>
                    <xsl:value-of select="longitude/degrees" />
                    °
                </span>
                <span>
                    <xsl:value-of select="longitude/minutes" />
                    ″
                </span>
                <span>
                    <xsl:value-of select="longitude/@orientation" />
                </span>
            </li>
        </ul>
    </xsl:template>

</xsl:stylesheet>
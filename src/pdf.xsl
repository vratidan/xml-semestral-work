<xsl:stylesheet version="1.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:fo="http://www.w3.org/1999/XSL/Format">
      <xsl:output method="xml" indent="yes"/>

    <xsl:template match="country">
    <fo:root>
        <fo:layout-master-set>
        <fo:simple-page-master master-name="A4-portrait"
                page-height="29.7cm" page-width="21.0cm" margin="2cm">
            <fo:region-body/>
        </fo:simple-page-master>
        </fo:layout-master-set>
        <fo:page-sequence master-reference="A4-portrait">
        <fo:flow flow-name="xsl-region-body">
            <fo:block text-align="center" padding-top="10pt">
                <fo:block>
                    <fo:external-graphic height="40px"
                                        content-width="scale-to-fit"
                                        content-height="scale-to-fit"
                                        scaling="uniform">
                        <xsl:attribute name="src">
                            url(./../out/<xsl:value-of select="//introduction/flag"/>)
                        </xsl:attribute>
                    </fo:external-graphic>
                </fo:block>
                <fo:block font-size="30pt" font-weight="400" text-align="center">
                    <xsl:value-of select="//country-name" />
                </fo:block>
                <fo:block font-size="15pt" font-weight="400" text-align="center">
                    <xsl:value-of select="//map-references" />
                </fo:block>
            </fo:block>

            <fo:block font-size="20pt" font-weight="400" text-align="center" margin-top='25pt'>
                    Introduction
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                        Background
                    </fo:block>
                <fo:block>
                    <xsl:value-of select="//introduction/background" />
                </fo:block>  
            </fo:block>

            <fo:block font-size="20pt" font-weight="400" text-align="center" margin-top='25pt'>
                    Geography
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                        Location
                    </fo:block>
                <fo:block>
                    <xsl:value-of select="//geography/location" />
                </fo:block>  
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                        Geographic coordinates
                    </fo:block>
                <fo:block>
                    <xsl:apply-templates select="//geography/geographic-coordinates" />
                </fo:block>  
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                    Map references
                    </fo:block>
                <fo:block>
                    <xsl:value-of select="//geography/map-references" />
                </fo:block>  
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                        Area
                    </fo:block>
                <fo:block>
                    <xsl:apply-templates select="//geography/area" />
                </fo:block>  
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                        Land boundaries
                    </fo:block>
                <fo:block>
                    Total: <xsl:value-of select="//geography/land-boundaries" /> <xsl:value-of select="//geography/land-boundaries/@unit" />
                </fo:block>  
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                        Coastline
                    </fo:block>
                <fo:block>
                    <xsl:value-of select="//geography/coastline" /> <xsl:value-of select="//geography/coastline/@unit" />
                </fo:block>  
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                        Climate
                    </fo:block>
                <fo:block>
                    <xsl:value-of select="//geography/climate" />
                </fo:block>  
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                        Terrain
                    </fo:block>
                <fo:block>
                    <xsl:value-of select="//geography/terrain" />
                </fo:block>  
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                        Elevation
                    </fo:block>
                <fo:block>
                    <xsl:apply-templates select="//geography/elevation" />
                </fo:block>  
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                        Natural resources
                    </fo:block>
                <fo:block>
                    <xsl:apply-templates select="//geography/natural-resources" />
                </fo:block>  
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                        Land uses
                    </fo:block>
                <fo:block>
                    <xsl:apply-templates select="//geography/land-uses" />
                </fo:block>  
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                        Population distribution
                    </fo:block>
                <fo:block>
                    <xsl:value-of select="//geography/population-distribution" />
                </fo:block>  
            </fo:block>


            <fo:block font-size="20pt" font-weight="400" text-align="center" margin-top='25pt'>
                People and Society
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                        Population
                    </fo:block>
                <fo:block>
                    <xsl:value-of select="//people-and-society/population" />' people ('<xsl:value-of select="//people-and-society/population/@date" />)
                </fo:block>  
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                        Ethnic groups
                    </fo:block>
                <fo:block>
                    <xsl:apply-templates select="//people-and-society/ethnic-groups" />
                </fo:block>  
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                        Languages
                    </fo:block>
                <fo:block>
                    <xsl:apply-templates select="//people-and-society/languages" />
                </fo:block>  
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                    Religion
                </fo:block>
                <fo:block>
                    <xsl:apply-templates select="//people-and-society/religions" />
                </fo:block>  
            </fo:block>


            <fo:block font-size="20pt" font-weight="400" text-align="center" margin-top='25pt'>
                Goverment
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                        County name
                    </fo:block>
                <fo:block>
                    <xsl:value-of select="//government/country-name" />
                </fo:block>  
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                        Goverment type
                    </fo:block>
                <fo:block>
                    <xsl:value-of select="//government/government-type" />
                </fo:block>  
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                        Capital
                    </fo:block>
                <fo:block>
                    <xsl:value-of select="//government/capital" />
                </fo:block>  
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                        Administrative divisions
                    </fo:block>
                <fo:block>
                    <xsl:value-of select="//government/administrative-divisions" />
                </fo:block>  
            </fo:block>

            <fo:block>
                <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                        National holiday
                    </fo:block>
                <fo:block>
                    <xsl:value-of select="//government/national-holiday" />
                </fo:block>  
            </fo:block>

        </fo:flow>
        </fo:page-sequence>
    </fo:root>
    </xsl:template>


    <xsl:template match="holiday">
        <fo:block>
            <xsl:value-of select="name" />: <xsl:value-of select="date" />
        </fo:block>
    </xsl:template>

    <xsl:template match="government/national-holiday">
        <fo:block>
            <xsl:apply-templates select="holiday" />
        </fo:block>
    </xsl:template>

    <xsl:template match="administrative-division">
        <fo:block>
            <xsl:value-of select="." />
        </fo:block>
    </xsl:template>

    <xsl:template match="government/administrative-divisions">
        <fo:block>
            <xsl:apply-templates select="administrative-division" />
        </fo:block>
    </xsl:template>

    <xsl:template match="government/capital">
        <fo:block>
            Name: <xsl:apply-templates select="name" />
        </fo:block>
        <fo:block>
            <fo:block font-size="15pt" font-weight="400" margin-top='10pt'>
                        Geographic coordinates
            </fo:block>
            <xsl:apply-templates select="geographic-coordinates" />
        </fo:block>
        <fo:block>
            Name: <xsl:apply-templates select="time-difference" />
        </fo:block>
    </xsl:template>

    <xsl:template match="religion">
        <fo:block>
            <xsl:value-of select="name" />: <xsl:value-of select="percentage" />%
        </fo:block>
    </xsl:template>

    <xsl:template match="people-and-society/religions">
        <fo:block>
            <xsl:apply-templates select="religion" />
        </fo:block>
    </xsl:template>

    <xsl:template match="language">
        <fo:block>
            <xsl:value-of select="." />
        </fo:block>
    </xsl:template>

    <xsl:template match="people-and-society/languages">
        <fo:block>
            <xsl:apply-templates select="language" />
        </fo:block>
    </xsl:template>

    <xsl:template match="ethnic-group">
        <fo:block>
            <xsl:value-of select="type" />: <xsl:value-of select="percentage" />%
        </fo:block>
    </xsl:template>

    <xsl:template match="people-and-society/ethnic-groups">
        <fo:block>
            <xsl:apply-templates select="ethnic-group" />
        </fo:block>
    </xsl:template>

    <xsl:template match="resource">
        <fo:block>
            <xsl:value-of select="name" />: <xsl:value-of select="percentage" />%
        </fo:block>
    </xsl:template>

    <xsl:template match="geography/land-uses">
        <fo:block>
            <xsl:apply-templates select="land-use" />
        </fo:block>
    </xsl:template>

    <xsl:template match="resource">
        <fo:block>
            <xsl:value-of select="." />
        </fo:block>
    </xsl:template>

    <xsl:template match="geography/natural-resources">
        <fo:block>
            <xsl:apply-templates select="resource" />
        </fo:block>
    </xsl:template>

    <xsl:template match="geography/elevation">
        <fo:block>
            Mean elevation: <xsl:value-of select="mean-elevation" /><xsl:value-of select="mean-elevation/@unit" />
        </fo:block>  
        <fo:block>
            Lowest point: <xsl:value-of select="lowest-point" /><xsl:value-of select="lowest-point/@unit" />
        </fo:block> 
        <fo:block>
            Highest point: <xsl:value-of select="highest-point" /><xsl:value-of select="highest-point/@unit" />
        </fo:block> 
    </xsl:template>

    <xsl:template match="geography/area">
        <fo:block>
            Land: <xsl:value-of select="land" /><xsl:value-of select="land/@unit" />
        </fo:block>  
        <fo:block>
            Water: <xsl:value-of select="water" /><xsl:value-of select="water/@unit" />
        </fo:block> 
    </xsl:template>

    <xsl:template match="geography/geographic-coordinates">
        <fo:block>
            Latitude: <xsl:value-of select="latitude/degrees" />° <xsl:value-of select="latitude/minutes" />' <xsl:value-of select="latitude/@orientation" />
        </fo:block>  
        <fo:block>
            Longitude: <xsl:value-of select="longitude/degrees" />° <xsl:value-of select="longitude/minutes" />' <xsl:value-of select="longitude/@orientation" />
        </fo:block> 
    </xsl:template>
    

</xsl:stylesheet>
#!/bin/bash

java -jar jing/bin/jing.jar src/definition.rng xml/Uruguay.xml
java -jar jing/bin/jing.jar src/definition.rng xml/Rwanda.xml
java -jar jing/bin/jing.jar src/definition.rng xml/Singapore.xml
java -jar jing/bin/jing.jar src/definition.rng xml/Norway.xml

java -jar saxon9he.jar dtd/UrugayDTD.xml src/template.xslt > out/Uruguay.html
java -jar saxon9he.jar dtd/RwandaDTD.xml src/template.xslt > out/Rwanda.html
java -jar saxon9he.jar dtd/SingaporeDTD.xml src/template.xslt > out/Singapore.html
java -jar saxon9he.jar dtd/NorwayDTD.xml src/template.xslt > out/Norway.html

java -jar saxon9he.jar xml/index.xml src/index.xslt > out/index.html

fop/fop/fop -xml Uruguay.xml -xsl pdf.xsl -pdf out/Uruguay.pdf
fop/fop/fop -xml Rwanda.xml -xsl pdf.xsl -pdf out/Rwanda.pdf
fop/fop/fop -xml Singapore.xml -xsl pdf.xsl -pdf out/Singapore.pdf
fop/fop/fop -xml Norway.xml -xsl pdf.xsl -pdf out/Norway.pdf
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE country SYSTEM "validate.dtd">
<country>
    <introduction>
        <flag>Uruguay-flag.gif</flag>
        <background>Montevideo, founded by the Spanish in 1726 as a military stronghold, soon took advantage of its natural harbor to become an important commercial center. Claimed by Argentina but annexed by Brazil in 1821, Uruguay declared its independence four years later and secured its freedom in 1828 after a three-year struggle. The administrations of President Jose BATLLE in the early 20th century launched widespread political, social, and economic reforms that established a statist tradition. A violent Marxist urban guerrilla movement named the Tupamaros, launched in the late 1960s, led Uruguay's president to cede control of the government to the military in 1973. By yearend, the rebels had been crushed, but the military continued to expand its hold over the government. Civilian rule was restored in 1985. In 2004, the left-of-center Frente Amplio Coalition won national elections that effectively ended 170 years of political control previously held by the Colorado and National (Blanco) parties. Uruguay's political and labor conditions are among the freest on the continent.</background>
    </introduction>
    <geography>
        <location>Southern South America, bordering the South Atlantic Ocean, between Argentina and Brazil</location>
        <geographic-coordinates>
            <latitude orientation='S'>
                <degrees>33</degrees>
                <minutes>00</minutes>
            </latitude>
            <longitude orientation='W'>
                <degrees>56</degrees>
                <minutes>00</minutes>
            </longitude>
        </geographic-coordinates>
        <map-references>South America</map-references>
        <area>
            <land unit='km'>176215</land>
            <water unit='km'>175015</water>
        </area>
        <land-boundaries>
            <total unit='km'>1591</total>
        </land-boundaries>
        <coastline unit='km'>660</coastline>
        <climate>warm temperate; freezing temperatures almost unknown</climate>
        <terrain>mostly rolling plains and low hills; fertile coastal lowland</terrain>
        <elevation>
            <mean-elevation unit='m'>109</mean-elevation>
            <lowest-point unit='m'>0</lowest-point>
            <highest-point unit='m'>514</highest-point>
        </elevation>
        <natural-resources>
            <resource>arable land</resource>
            <resource>hydropower</resource>
            <resource>minor minerals</resource>
            <resource>fish</resource>
        </natural-resources>
        <land-uses>
            <land-use>
                <name>agricultural-land</name>
                <percentage>87.2</percentage>
            </land-use>
            <land-use>
                <name>forest</name>
                <percentage>10.2</percentage>
            </land-use>
            <land-use>
                <name>other</name>
                <percentage>2.6</percentage>
            </land-use>
        </land-uses>
        <population-distribution>most of the country's population resides in the southern half of the country; approximately 80% of the populace is urban, living in towns or cities; nearly half of the population lives in and around the capital of Montevideo</population-distribution>
    </geography>
    <people-and-society>
        <population date='July 2020'>3387605</population>
        <ethnic-groups year='2011'>
            <ethnic-group>
                <type>while</type>
                <percentage>87.7</percentage>
            </ethnic-group>
            <ethnic-group>
                <type>black</type>
                <percentage>4.6</percentage>
            </ethnic-group>
            <ethnic-group>
                <type>indigenous</type>
                <percentage>2.4</percentage>
            </ethnic-group>
            <ethnic-group>
                <type>other</type>
                <percentage>0.3</percentage>
            </ethnic-group>
        </ethnic-groups>
        <languages>
            <language official='true'>Spanish</language>
        </languages>
        <religions>
            <religion>
                <name>Roman Catholic</name>
                <percentage>47.1</percentage>
            </religion>
            <religion>
                <name>non-Catholic Christians</name>
                <percentage>11.1</percentage>
            </religion>
            <religion>
                <name>nondenominational</name>
                <percentage>23.2</percentage>
            </religion>
            <religion>
                <name>Jewish</name>
                <percentage>0.3</percentage>
            </religion>
            <religion>
                <name>atheist or agnostic</name>
                <percentage>17.2</percentage>
            </religion>
            <religion>
                <name>other</name>
                <percentage>1.1</percentage>
            </religion>
        </religions>
    </people-and-society>
    <government>
        <country-name>Uruguay</country-name>
        <government-type>presidential republic</government-type>
        <capital>
            <name>Montevideo</name>
            <geographic-coordinates>
                <latitude orientation='S'>
                    <degrees>34</degrees>
                    <minutes>51</minutes>
                </latitude>
                <longitude orientation='W'>
                    <degrees>56</degrees>
                    <minutes>10</minutes>
                </longitude>
            </geographic-coordinates>
            <time-difference>UTC-3</time-difference>
        </capital>
        <administrative-divisions>
            <department>Artigas</department>
            <department>Canelones</department>
            <department>Cerro Largo</department>
            <department>Colonia</department>
            <department>Durazno</department>
            <department>Flores</department>
            <department>Florida</department>
            <department>Lavalleja</department>
            <department>Maldonado</department>
            <department>Montevideo</department>
            <department>Paysandu</department>
            <department>Rio Negro</department>
            <department>Rivera</department>
            <department>Rocha</department>
            <department>Salto</department>
            <department>San Jose</department>
            <department>Soriano</department>
            <department>Tacuarembo</department>
            <department>Treinta y Tres</department>
        </administrative-divisions>
        <national-holiday>
            <holiday>
                <name>Independence Day</name>
                <date>25 August</date>
            </holiday>
        </national-holiday>
    </government>
    <economy>
        <economy-overview>
            <paragraph>Uruguay has a free market economy characterized by an export-oriented agricultural sector, a well-educated workforce, and high levels of social spending. Uruguay has sought to expand trade within the Common Market of the South (Mercosur) and with non-Mercosur members, and President VAZQUEZ has maintained his predecessor's mix of pro-market policies and a strong social safety net.</paragraph>
            <paragraph>Following financial difficulties in the late 1990s and early 2000s, Uruguay's economic growth averaged 8% annually during the 2004-08 period. The 2008-09 global financial crisis put a brake on Uruguay's vigorous growth, which decelerated to 2.6% in 2009. Nevertheless, the country avoided a recession and kept growth rates positive, mainly through higher public expenditure and investment; GDP growth reached 8.9% in 2010 but slowed markedly in the 2012-16 period as a result of a renewed slowdown in the global economy and in Uruguay's main trade partners and Mercosur counterparts, Argentina and Brazil. Reforms in those countries should give Uruguay an economic boost. Growth picked up in 2017.</paragraph>
        </economy-overview>
        <agriculture-products>
            <agriculture-product>
                <name>agriculture</name>
                <percentage>6.2</percentage>
            </agriculture-product>
            <agriculture-product>
                <name>industry</name>
                <percentage>24.1</percentage>
            </agriculture-product>
            <agriculture-product>
                <name>services</name>
                <percentage>69.7</percentage>
            </agriculture-product>
        </agriculture-products>
        <industries>
            <industry>food processing</industry>
            <industry>electrical machinery</industry>
            <industry>transportation equipment</industry>
            <industry>petroleum products</industry>
            <industry>textiles</industry>
            <industry>chemicals</industry>
            <industry>beverages</industry>
        </industries>
    </economy>
    <energy>
        <electricity-access>
            <electrification unit='%'>100</electrification>
        </electricity-access>
        <electricity-production unit='kWh'>13.13 billion</electricity-production>
        <electricity-consumption unit='kWh'>10.77 billion</electricity-consumption>
        <electricity-exports unit='kWh'>1.321 billion</electricity-exports>
        <electricity-imports unit='kWh'>24 million</electricity-imports>
        <electricity-installed-generating-capacity unit='kWh'>4.808 million</electricity-installed-generating-capacity>
    </energy>
    <communications>
        <telephones-fixed-lines>
            <total-subscriptions>1153533</total-subscriptions>
        </telephones-fixed-lines>
        <telephones-mobile-cellular>
            <total-subscriptions>5170624</total-subscriptions>
        </telephones-mobile-cellular>
        <telephone-system>
            <system>
                <name>general assessment</name>
                <description year='2018'>fully digitalized; one of the highest broadband penetrations in Latin America; high fixed-line and mobile penetrations as well; FttP coverage by 2022; nationwide 3G coverage</description>
            </system>
            <system>
                <name>domestic</name>
                <description year='2018'>most modern facilities concentrated in Montevideo; nationwide microwave radio relay network; overall fixed-line 34 per 100 and mobile-cellular teledensity 153 per 100 persons</description>
            </system>
            <system>
                <name>international</name>
                <description year='2020'>country code - 598; landing points for the Unisor, Tannat, and Bicentenario submarine cable system providing direct connectivity to Brazil and Argentina; Bicentenario 2012 and Tannat 2017 cables helped end-users with Internet bandwidth; satellite earth stations - 2 Intelsat (Atlantic Ocean)</description>
            </system>
        </telephone-system>
        <broadcast-media year='2019'>mixture of privately owned and state-run broadcast media; more than 100 commercial radio stations and about 20 TV channels; cable TV is available; many community radio and TV stations; adopted the hybrid Japanese/Brazilian HDTV standard (ISDB-T) in December 2010</broadcast-media>
        <internet-country-code>.uy</internet-country-code>
        <internet-users>
            <total>2225075</total>
            <percent-of-population>66.4</percent-of-population>
        </internet-users>
        <broadband-fixed-subscriptions>
            <total>977390</total>
        </broadband-fixed-subscriptions>
    </communications>
    <military-and-security>
        <military-expenditures>
            <expenditure year='2018'>1.95</expenditure>
            <expenditure year='2017'>1.98</expenditure>
            <expenditure year='2016'>1.88</expenditure>
            <expenditure year='2015'>1.82</expenditure>
            <expenditure year='2014'>1.81</expenditure>
        </military-expenditures>
        <military-service-age-and-obligation year='2013'>18-30 years of age (18-22 years of age for Navy) for male or female voluntary military service; up to 40 years of age for specialists; enlistment is voluntary in peacetime, but the government has the authority to conscript in emergencies</military-service-age-and-obligation>
    </military-and-security>
    <transportation>
        <national-air-transport-system>
            <registered-air-carriers year='2015'>2</registered-air-carriers>
        </national-air-transport-system>
        <civil-aircraft-registration-country-code-prefix>CX</civil-aircraft-registration-country-code-prefix>
        <airports>133</airports>
        <pipelines>
            <gas unit='km'>257</gas>
            <oil unit='km'>160</oil>
        </pipelines>
        <railways unit='km'>1673</railways>
        <roadways unit='km'>77732</roadways>
        <waterways unit='km'>1600</waterways>
        <merchant-marine>60</merchant-marine>
        <ports-and-terminals>
            <port-terminal>Montevideo</port-terminal>
        </ports-and-terminals>
    </transportation>
    <transnational-issues>
        <disputes-international>in 2010, the ICJ ruled in favor of Uruguay's operation of two paper mills on the Uruguay River, which forms the border with Argentina; the two countries formed a joint pollution monitoring regime; uncontested boundary dispute between Brazil and Uruguay over Braziliera/Brasiliera Island in the Quarai/Cuareim River leaves the tripoint with Argentina in question; smuggling of firearms and narcotics continues to be an issue along the Uruguay-Brazil border</disputes-international>
        <refugees-and-internally-displaced-persons>
            <refugees>13694</refugees>
        </refugees-and-internally-displaced-persons>
        <illicit-drugs>small-scale transit country for drugs mainly bound for Europe, often through sea-borne containers; law enforcement corruption; money laundering because of strict banking secrecy laws; weak border control along Brazilian frontier; increasing consumption of cocaine base and synthetic drugs</illicit-drugs>
    </transnational-issues>
</country>
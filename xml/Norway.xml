<?xml version="1.0" encoding="UTF-8"?>

<country>
    <introduction>
        <flag>Norway-flag.gif</flag>
        <background>Two centuries of Viking raids into Europe tapered off following the adoption of Christianity by King Olav TRYGGVASON in 994; conversion of the Norwegian kingdom occurred over the next several decades. In 1397, Norway was absorbed into a union with Denmark that lasted more than four centuries. In 1814, Norwegians resisted the cession of their country to Sweden and adopted a new constitution. Sweden then invaded Norway but agreed to let Norway keep its constitution in return for accepting the union under a Swedish king. Rising nationalism throughout the 19th century led to a 1905 referendum granting Norway independence. Although Norway remained neutral in World War I, it suffered heavy losses to its shipping. Norway proclaimed its neutrality at the outset of World War II, but was nonetheless occupied for five years by Nazi Germany (1940-45). In 1949, Norway abandoned neutrality and became a member of NATO. Discovery of oil and gas in adjacent waters in the late 1960s boosted Norway's economic fortunes. In referenda held in 1972 and 1994, Norway rejected joining the EU. Key domestic issues include immigration and integration of ethnic minorities, maintaining the country's extensive social safety net with an aging population, and preserving economic competitiveness.</background>
    </introduction>
    <geography>
        <location>Northern Europe, bordering the North Sea and the North Atlantic Ocean, west of Sweden</location>
        <geographic-coordinates>
            <latitude orientation='N'>
                <degrees>62</degrees>
                <minutes>00</minutes>
            </latitude>
            <longitude orientation='E'>
                <degrees>10</degrees>
                <minutes>00</minutes>
            </longitude>
        </geographic-coordinates>
        <map-references>Europe</map-references>
        <area>
            <land unit='km'>304282</land>
            <water unit='km'>19520</water>
        </area>
        <land-boundaries>
            <total unit='km'>2566</total>
        </land-boundaries>
        <coastline unit='km'>25148</coastline>
        <climate>temperate along coast, modified by North Atlantic Current; colder interior with increased precipitation and colder summers; rainy year-round on west coast</climate>
        <terrain>glaciated; mostly high plateaus and rugged mountains broken by fertile valleys; small, scattered plains; coastline deeply indented by fjords; arctic tundra in north</terrain>
        <elevation>
            <mean-elevation unit='m'>460</mean-elevation>
            <lowest-point unit='m'>0</lowest-point>
            <highest-point unit='m'>2469</highest-point>
        </elevation>
        <natural-resources>
            <resource>petroleum</resource>
            <resource>natural gas</resource>
            <resource>iron ore</resource>
            <resource>fish</resource>
            <resource>copper</resource>
            <resource>lead</resource>
            <resource>zinc</resource>
            <resource>titanium</resource>
            <resource>pyrites</resource>
            <resource>nickel</resource>
            <resource>timber</resource>
            <resource>hydropower</resource>
        </natural-resources>
        <land-uses>
            <land-use>
                <name>agricultural-land</name>
                <percentage>2.7</percentage>
            </land-use>
            <land-use>
                <name>forest</name>
                <percentage>27.8</percentage>
            </land-use>
            <land-use>
                <name>other</name>
                <percentage>69.5</percentage>
            </land-use>
        </land-uses>
        <population-distribution>most Norweigans live in the south where the climate is milder and there is better connectivity to mainland Europe; population clusters are found all along the North Sea coast in the southwest, and Skaggerak in the southeast; the interior areas of the north remain sparsely populated</population-distribution>
    </geography>
    <people-and-society>
        <population date='July 2020'>5467439</population>
        <ethnic-groups year='2011'>
            <ethnic-group>
                <type>Norwegian</type>
                <percentage>83.2</percentage>
            </ethnic-group>
            <ethnic-group>
                <type>other European</type>
                <percentage>8.3</percentage>
            </ethnic-group>
            <ethnic-group>
                <type>other</type>
                <percentage>8.5</percentage>
            </ethnic-group>
        </ethnic-groups>
        <languages>
            <language official='true'>Bokmal Norwegian</language>
            <language official='true'>Nynorsk Norwegian</language>
        </languages>
        <religions>
            <religion>
                <name>Church of Norway</name>
                <percentage>70.6</percentage>
            </religion>
            <religion>
                <name>Muslim</name>
                <percentage>3.2</percentage>
            </religion>
            <religion>
                <name>Roman Catholic</name>
                <percentage>3</percentage>
            </religion>
            <religion>
                <name>other Christian</name>
                <percentage>3.7</percentage>
            </religion>
            <religion>
                <name>other</name>
                <percentage>2.5</percentage>
            </religion>
        </religions>
    </people-and-society>
    <government>
        <country-name>Norway</country-name>
        <government-type>parliamentary constitutional monarchy</government-type>
        <capital>
            <name>Oslo</name>
            <geographic-coordinates>
                <latitude orientation='N'>
                    <degrees>59</degrees>
                    <minutes>55</minutes>
                </latitude>
                <longitude orientation='E'>
                    <degrees>10</degrees>
                    <minutes>45</minutes>
                </longitude>
            </geographic-coordinates>
            <time-difference>UTC+1</time-difference>
        </capital>
        <administrative-divisions>
            <department>Akershus</department>
            <department>Aust-Agder</department>
            <department>Buskerud</department>
            <department>Finnmark</department>
            <department>Hedmark</department>
            <department>Hordaland</department>
            <department>More og Romsdal</department>
            <department>Nordland</department>
            <department>Oppland</department>
            <department>Oslo</department>
            <department>Ostfold</department>
            <department>Rogaland</department>
            <department>Sogn og Fjordane</department>
            <department>Telemark</department>
            <department>Troms</department>
            <department>Trondelag</department>
            <department>Soriano</department>
            <department>Vest-Agder</department>
            <department>Vestfold</department>
        </administrative-divisions>
        <national-holiday>
            <holiday>
                <name>Constitution Day</name>
                <date>17 May</date>
            </holiday>
        </national-holiday>
    </government>
    <economy>
        <economy-overview>
            <paragraph>Norway has a stable economy with a vibrant private sector, a large state sector, and an extensive social safety net. Norway opted out of the EU during a referendum in November 1994. However, as a member of the European Economic Area, Norway partially participates in the EU’s single market and contributes sizably to the EU budget.</paragraph>
            <paragraph>The country is richly endowed with natural resources such as oil and gas, fish, forests, and minerals. Norway is a leading producer and the world’s second largest exporter of seafood, after China. The government manages the country’s petroleum resources through extensive regulation. The petroleum sector provides about 9% of jobs, 12% of GDP, 13% of the state’s revenue, and 37% of exports, according to official national estimates. Norway is one of the world's leading petroleum exporters, although oil production is close to 50% below its peak in 2000. Gas production, conversely, has more than doubled since 2000. Although oil production is historically low, it rose in 2016 for the third consecutive year due to the higher production of existing oil fields and to new fields coming on stream. Norway’s domestic electricity production relies almost entirely on hydropower.</paragraph>
            <paragraph>In anticipation of eventual declines in oil and gas production, Norway saves state revenue from petroleum sector activities in the world's largest sovereign wealth fund, valued at over $1 trillion at the end of 2017. To help balance the federal budget each year, the government follows a "fiscal rule," which states that spending of revenues from petroleum and fund investments shall correspond to the expected real rate of return on the fund, an amount it estimates is sustainable over time. In February 2017, the government revised the expected rate of return for the fund downward from 4% to 3%.</paragraph>
            <paragraph>After solid GDP growth in the 2004-07 period, the economy slowed in 2008, and contracted in 2009, before returning to modest, positive growth from 2010 to 2017. The Norwegian economy has been adjusting to lower energy prices, as demonstrated by growth in labor force participation and employment in 2017. GDP growth was about 1.5% in 2017, driven largely by domestic demand, which has been boosted by the rebound in the labor market and supportive fiscal policies. Economic growth is expected to remain constant or improve slightly in the next few years.</paragraph>
        </economy-overview>
        <agriculture-products>
            <agriculture-product>
                <name>barley</name>
            </agriculture-product>
            <agriculture-product>
                <name>wheat</name>
            </agriculture-product>
            <agriculture-product>
                <name>potatoes</name>
            </agriculture-product>
            <agriculture-product>
                <name>pork</name>
            </agriculture-product>
            <agriculture-product>
                <name>beef</name>
            </agriculture-product>
            <agriculture-product>
                <name>veal</name>
            </agriculture-product>
            <agriculture-product>
                <name>milk</name>
            </agriculture-product>
            <agriculture-product>
                <name>fish</name>
            </agriculture-product>
        </agriculture-products>
        <industries>
            <industry>petroleum and gas</industry>
            <industry>shipping</industry>
            <industry>fishing</industry>
            <industry>aquaculture</industry>
            <industry>food processing</industry>
            <industry>shipbuilding</industry>
            <industry>pulp and paper products</industry>
            <industry>metals</industry>
            <industry>chemicals</industry>
            <industry>timber</industry>
            <industry>mining</industry>
            <industry>textiles</industry>
        </industries>
    </economy>
    <energy>
        <electricity-access>
            <electrification unit='%'>100</electrification>
        </electricity-access>
        <electricity-production unit='kWh'>147.7 billion</electricity-production>
        <electricity-consumption unit='kWh'>122.2 billion</electricity-consumption>
        <electricity-exports unit='kWh'>15.53 billion</electricity-exports>
        <electricity-imports unit='kWh'>5.741 billion</electricity-imports>
        <electricity-installed-generating-capacity unit='kWh'>33.86 million</electricity-installed-generating-capacity>
    </energy>
    <communications>
        <telephones-fixed-lines>
            <total-subscriptions>560945</total-subscriptions>
        </telephones-fixed-lines>
        <telephones-mobile-cellular>
            <total-subscriptions>5720892</total-subscriptions>
        </telephones-mobile-cellular>
        <telephone-system>
            <system>
                <name>general assessment</name>
                <description year='2018'>modern in all respects; one of the most advanced telecommunications networks in Europe; forward leaning in LTE-A developments; looking to close 3G and 2G networks by 2025 and preparing for 5G; broadband penetration rate is among the best in Europe</description>
            </system>
            <system>
                <name>domestic</name>
                <description year='2018'>Norway has a domestic satellite system; the prevalence of rural areas encourages the wide use of mobile-cellular systems; fixed-line 10 per 100 and mobile-cellular 106 per 100</description>
            </system>
            <system>
                <name>international</name>
                <description year='2019'>country code - 47; landing points for the Svalbard Undersea Cable System, Polar Circle Cable, Bodo-Rost Cable, NOR5KE Viking, Celtic Norse, Tempnet Offshore FOC Network, England Cable, Denmark-Norwary6, Havfrue/AEC-2, Skagerrak 4, and the Skagenfiber West and East submarine cables providing links to other Nordic countries, Europe and the US; satellite earth stations - Eutelsat, Intelsat (Atlantic Ocean), and 1 Inmarsat (Atlantic and Indian Ocean regions); note - Norway shares the Inmarsat earth station with the other Nordic countries (Denmark, Finland, Iceland, and Sweden)</description>
            </system>
        </telephone-system>
        <broadcast-media year='2019'>state-owned public radio-TV broadcaster operates 3 nationwide TV stations, 3 nationwide radio stations, and 16 regional radio stations; roughly a dozen privately owned TV stations broadcast nationally and roughly another 25 local TV stations broadcasting; nearly 75% of households have access to multi-channel cable or satellite TV; 2 privately owned radio stations broadcast nationwide and another 240 stations operate locally; Norway is the first country in the world to phase out FM radio in favor of Digital Audio Broadcasting (DAB), a process scheduled for completion in late 2017</broadcast-media>
        <internet-country-code>.no</internet-country-code>
        <internet-users>
            <total>5122904</total>
            <percent-of-population>97.3</percent-of-population>
        </internet-users>
        <broadband-fixed-subscriptions>
            <total>2206519</total>
        </broadband-fixed-subscriptions>
    </communications>
    <military-and-security>
        <military-expenditures>
            <expenditure year='2018'>1.8</expenditure>
            <expenditure year='2017'>1.73</expenditure>
            <expenditure year='2016'>1.71</expenditure>
            <expenditure year='2015'>1.73</expenditure>
            <expenditure year='2014'>1.59</expenditure>
        </military-expenditures>
        <military-service-age-and-obligation year='2019'>19-35 years of age for male and female selective compulsory military service; 17 years of age for male volunteers (16 in wartime); 18 years of age for women; 19-month service obligation; conscripts first serve 12 months from 19-28, and then up to 4-5 refresher training periods until age 35, 44, 55, or 60 depending on rank and function.</military-service-age-and-obligation>
    </military-and-security>
    <transportation>
        <national-air-transport-system>
            <registered-air-carriers year='2015'>3</registered-air-carriers>
        </national-air-transport-system>
        <civil-aircraft-registration-country-code-prefix>LN</civil-aircraft-registration-country-code-prefix>
        <airports>95</airports>
        <pipelines>
            <gas unit='km'>8520</gas>
            <oil unit='km'>1304</oil>
        </pipelines>
        <railways unit='km'>4200</railways>
        <roadways unit='km'>94902</roadways>
        <waterways unit='km'>16157700</waterways>
        <merchant-marine>1576</merchant-marine>
        <ports-and-terminals>
            <port-terminal>Bergen</port-terminal>
            <port-terminal>Haugesund</port-terminal>
            <port-terminal>Maaloy</port-terminal>
            <port-terminal>Mongstad</port-terminal>
            <port-terminal>Narvik</port-terminal>
            <port-terminal>Sture</port-terminal>
        </ports-and-terminals>
    </transportation>
    <transnational-issues>
        <disputes-international>Norway asserts a territorial claim in Antarctica (Queen Maud Land and its continental shelf); Denmark (Greenland) and Norway have made submissions to the Commission on the Limits of the Continental Shelf (CLCS) and Russia is collecting additional data to augment its 2001 CLCS submission; Norway and Russia signed a comprehensive maritime boundary agreement in 2010</disputes-international>
        <refugees-and-internally-displaced-persons>
            <refugees>15246</refugees>
        </refugees-and-internally-displaced-persons>
    </transnational-issues>
</country>
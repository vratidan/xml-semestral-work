<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html" encoding="utf-8" indent="yes" />

    <xsl:template match="countries">
        <html>
            <head>

            </head>
            <body>
                <ul>
                    <xsl:apply-templates select="country" />
                </ul>
            </body>
        </html>
    </xsl:template>

    <xsl:template match='country'>
    <li>    
    <a>
            <xsl:attribute name="href"><xsl:value-of select="."/>.html</xsl:attribute>
            <xsl:value-of select="."/>
        </a>
    </li>
    </xsl:template>

</xsl:stylesheet>
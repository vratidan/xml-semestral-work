<?xml version="1.0" encoding="UTF-8"?>

<country>
    <introduction>
        <flag>Rwanda-flag.gif</flag>
        <background>In 1959, three years before independence from Belgium, the majority ethnic group, the Hutus, overthrew the ruling Tutsi king. Over the next several years, thousands of Tutsis were killed, and some 150,000 driven into exile in neighboring countries. The children of these exiles later formed a rebel group, the Rwandan Patriotic Front (RPF), and began a civil war in 1990. The war, along with several political and economic upheavals, exacerbated ethnic tensions, culminating in April 1994 in a state-orchestrated genocide, in which Rwandans killed approximately 800,000 of their fellow citizens, including approximately three-quarters of the Tutsi population. The genocide ended later that same year when the predominantly Tutsi RPF, operating out of Uganda and northern Rwanda, defeated the national army and Hutu militias, and established an RPF-led government of national unity. Rwanda held its first local elections in 1999 and its first post-genocide presidential and legislative elections in 2003. Rwanda joined the Commonwealth in late 2009. President Paul KAGAME won the presidential election in August 2017 after changing the constitution in 2016 to allow him to run for a third term.</background>
    </introduction>
    <geography>
        <location>Central Africa, east of the Democratic Republic of the Congo, north of Burundi</location>
        <geographic-coordinates>
            <latitude orientation='S'>
                <degrees>2</degrees>
                <minutes>00</minutes>
            </latitude>
            <longitude orientation='E'>
                <degrees>30</degrees>
                <minutes>00</minutes>
            </longitude>
        </geographic-coordinates>
        <map-references>Africa</map-references>
        <area>
            <land unit='km'>24668</land>
            <water unit='km'>1670</water>
        </area>
        <land-boundaries>
            <total unit='km'>930</total>
        </land-boundaries>
        <coastline unit='km'>0</coastline>
        <climate>temperate; two rainy seasons (February to April, November to January); mild in mountains with frost and snow possible</climate>
        <terrain>mostly grassy uplands and hills; relief is mountainous with altitude declining from west to east</terrain>
        <elevation>
            <mean-elevation unit='m'>1598</mean-elevation>
            <lowest-point unit='m'>950</lowest-point>
            <highest-point unit='m'>4519</highest-point>
        </elevation>
        <natural-resources>
            <resource>gold</resource>
            <resource>cassiterite</resource>
            <resource>wolframite</resource>
            <resource>methane</resource>
            <resource>hydropower</resource>
            <resource>arable land</resource>
        </natural-resources>
        <land-uses>
            <land-use>
                <name>agricultural-land</name>
                <percentage>74.5</percentage>
            </land-use>
            <land-use>
                <name>forest</name>
                <percentage>18</percentage>
            </land-use>
            <land-use>
                <name>other</name>
                <percentage>7.5</percentage>
            </land-use>
        </land-uses>
        <population-distribution>one of Africa's most densely populated countries; large concentrations tend to be in the central regions and along the shore of Lake Kivu in the west</population-distribution>
    </geography>
    <people-and-society>
        <population date='July 2020'>12712431</population>
        <ethnic-groups year='2020'>
            <ethnic-group>
                <type>Hutu</type>
            </ethnic-group>
            <ethnic-group>
                <type>Tutsi</type>
            </ethnic-group>
            <ethnic-group>
                <type>Twa</type>
            </ethnic-group>
        </ethnic-groups>
        <languages>
            <language>Kinyarwanda</language>
            <language official='true'>French</language>
            <language official='true'>English</language>
            <language official='true'>other</language>
        </languages>
        <religions>
            <religion>
                <name>Protestant</name>
                <percentage>49.5</percentage>
            </religion>
            <religion>
                <name>Roman Catholic</name>
                <percentage>43.7</percentage>
            </religion>
            <religion>
                <name>Muslim</name>
                <percentage>2</percentage>
            </religion>
            <religion>
                <name>other</name>
                <percentage>0.9</percentage>
            </religion>
        </religions>
    </people-and-society>
    <government>
        <country-name>Rwanda</country-name>
        <government-type>presidential republic</government-type>
        <capital>
            <name>Kigali</name>
            <geographic-coordinates>
                <latitude orientation='S'>
                    <degrees>1</degrees>
                    <minutes>57</minutes>
                </latitude>
                <longitude orientation='E'>
                    <degrees>30</degrees>
                    <minutes>03</minutes>
                </longitude>
            </geographic-coordinates>
            <time-difference>UTC+2</time-difference>
        </capital>
        <administrative-divisions>
            <department>Est</department>
            <department>Nord</department>
            <department>Ouest</department>
            <department>Sud</department>
        </administrative-divisions>
        <national-holiday>
            <holiday>
                <name>Independence Day</name>
                <date>1 July</date>
            </holiday>
        </national-holiday>
    </government>
    <economy>
        <economy-overview>
            <paragraph>Rwanda is a rural, agrarian country with agriculture accounting for about 63% of export earnings, and with some mineral and agro-processing. Population density is high but, with the exception of the capital Kigali, is not concentrated in large cities – its 12 million people are spread out on a small amount of land (smaller than the state of Maryland). Tourism, minerals, coffee, and tea are Rwanda's main sources of foreign exchange. Despite Rwanda's fertile ecosystem, food production often does not keep pace with demand, requiring food imports. Energy shortages, instability in neighboring states, and lack of adequate transportation linkages to other countries continue to handicap private sector growth.</paragraph>
            <paragraph>The 1994 genocide decimated Rwanda's fragile economic base, severely impoverished the population, particularly women, and temporarily stalled the country's ability to attract private and external investment. However, Rwanda has made substantial progress in stabilizing and rehabilitating its economy well beyond pre-1994 levels. GDP has rebounded with an average annual growth of 6%-8% since 2003 and inflation has been reduced to single digits. In 2015, 39% of the population lived below the poverty line, according to government statistics, compared to 57% in 2006.</paragraph>
            <paragraph>The government has embraced an expansionary fiscal policy to reduce poverty by improving education, infrastructure, and foreign and domestic investment. Rwanda consistently ranks well for ease of doing business and transparency.</paragraph>
            <paragraph>The Rwandan Government is seeking to become a regional leader in information and communication technologies and aims to reach middle-income status by 2020 by leveraging the service industry. In 2012, Rwanda completed the first modern Special Economic Zone (SEZ) in Kigali. The SEZ seeks to attract investment in all sectors, but specifically in agribusiness, information and communications, trade and logistics, mining, and construction. In 2016, the government launched an online system to give investors information about public land and its suitability for agricultural development.</paragraph>
        </economy-overview>
        <agriculture-products>
            <agriculture-product>
                <name>coffee</name>
            </agriculture-product>
            <agriculture-product>
                <name>tea</name>
            </agriculture-product>
            <agriculture-product>
                <name>pyrethrum</name>
            </agriculture-product>
            <agriculture-product>
                <name>bananas</name>
            </agriculture-product>
            <agriculture-product>
                <name>beans</name>
            </agriculture-product>
            <agriculture-product>
                <name>sorghum</name>
            </agriculture-product>
            <agriculture-product>
                <name>potatoes</name>
            </agriculture-product>
            <agriculture-product>
                <name>livestock</name>
            </agriculture-product>
        </agriculture-products>
        <industries>
            <industry>cement</industry>
            <industry>agricultural products</industry>
            <industry>small-scale beverages</industry>
            <industry>soap</industry>
            <industry>furniture</industry>
            <industry>shoes</industry>
            <industry>plastic goods</industry>
            <industry>textiles</industry>
            <industry>cigarettes</industry>
        </industries>
    </economy>
    <energy>
        <electricity-access>
            <electrification unit='%'>43</electrification>
        </electricity-access>
        <electricity-production unit='kWh'>525 million</electricity-production>
        <electricity-consumption unit='kWh'>527.3 million</electricity-consumption>
        <electricity-exports unit='kWh'>4 million</electricity-exports>
        <electricity-imports unit='kWh'>42 million</electricity-imports>
        <electricity-installed-generating-capacity unit='kWh'>191000</electricity-installed-generating-capacity>
    </energy>
    <communications>
        <telephones-fixed-lines>
            <total-subscriptions>12960</total-subscriptions>
        </telephones-fixed-lines>
        <telephones-mobile-cellular>
            <total-subscriptions>9700609</total-subscriptions>
        </telephones-mobile-cellular>
        <telephone-system>
            <system>
                <name>general assessment</name>
                <description year='2018'>small, inadequate telephone system that primarily serves business, education, and government; government carries out investment in smart city infrastructure; expands wholesale LTE services</description>
            </system>
            <system>
                <name>domestic</name>
                <description year='2018'>the capital, Kigali, is connected to provincial centers by microwave radio relay and recently, by cellular telephone service; much of the network depends on wire and HF radiotelephone; fixed-line less than 1 per 100 and mobile-cellular telephone density has increased to 80 telephones per 100 persons</description>
            </system>
            <system>
                <name>international</name>
                <description year='2020'>country code - 250; international connections employ microwave radio relay to neighboring countries and satellite communications to more distant countries; satellite earth stations - 1 Intelsat (Indian Ocean) in Kigali (includes telex and telefax service); international submarine fiber-optic cables on the African east coast has brought international bandwidth and lessened the dependency on satellites</description>
            </system>
        </telephone-system>
        <broadcast-media year='2019'>13 TV stations; 35 radio stations registered, including international broadcasters, government owns most popular TV and radio stations; regional satellite-based TV services available</broadcast-media>
        <internet-country-code>.rw</internet-country-code>
        <internet-users>
            <total>3724678</total>
            <percent-of-population>29.8</percent-of-population>
        </internet-users>
        <broadband-fixed-subscriptions>
            <total>7501</total>
        </broadband-fixed-subscriptions>
    </communications>
    <military-and-security>
        <military-expenditures>
            <expenditure year='2018'>1.23</expenditure>
            <expenditure year='2017'>1.28</expenditure>
            <expenditure year='2016'>1.28</expenditure>
            <expenditure year='2015'>1.25</expenditure>
            <expenditure year='2014'>1.13</expenditure>
        </military-expenditures>
        <military-service-age-and-obligation year='2013'>18 years of age for voluntary military service; no conscription; Rwandan citizenship is required, as is a 9th-grade education for enlisted recruits and an A-level certificate for officer candidates; enlistment is either as contract (5-years, renewable twice) or career; retirement (for officers and senior NCOs) after 20 years of service or at 40-60 years of age (2013)</military-service-age-and-obligation>
    </military-and-security>
    <transportation>
        <national-air-transport-system>
            <registered-air-carriers year='2015'>1</registered-air-carriers>
        </national-air-transport-system>
        <civil-aircraft-registration-country-code-prefix>9XR</civil-aircraft-registration-country-code-prefix>
        <airports>7</airports>
        <ports-and-terminals>
            <port-terminal>Cyangugu</port-terminal>
            <port-terminal>Gisenyi</port-terminal>
            <port-terminal>Kibuye</port-terminal>
        </ports-and-terminals>
    </transportation>
    <transnational-issues>
        <disputes-international>Burundi and Rwanda dispute two sq km (0.8 sq mi) of Sabanerwa, a farmed area in the Rukurazi Valley where the Akanyaru/Kanyaru River shifted its course southward after heavy rains in 1965; fighting among ethnic groups - loosely associated political rebels, armed gangs, and various government forces in Great Lakes region transcending the boundaries of Burundi, Democratic Republic of the Congo (DROC), Rwanda, and Uganda - abated substantially from a decade ago due largely to UN peacekeeping, international mediation, and efforts by local governments to create civil societies; nonetheless, 57,000 Rwandan refugees still reside in 21 African states, including Zambia, Gabon, and 20,000 who fled to Burundi in 2005 and 2006 to escape drought and recriminations from traditional courts investigating the 1994 massacres; the 2005 DROC and Rwanda border verification mechanism to stem rebel actions on both sides of the border remains in place</disputes-international>
        <refugees-and-internally-displaced-persons>
            <refugees>76266</refugees>
        </refugees-and-internally-displaced-persons>
    </transnational-issues>
</country>